'use strict';
let fs = require("fs");
let Web3 = require('web3');
var Pudding = require('ether-pudding');
var solc = require('solc');

var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
//console.log(web3.isConnected());

let source = fs.readFileSync("contracts/ballot.json");
console.log(">>>" + source);

let contracts = JSON.parse(source)["contracts"];
console.log(contracts);
let abi = JSON.parse(contracts['Ballot.sol:Ballot'].abi);

// Smart contract EVM bytecode as hex
let code = '0x' + contracts['Ballot.sol:Ballot'].bin;

var contract_data = {
    abi: abi,
    binary: code
};

console.log(web3.eth);
var _proposals = ['Proposal 1', 'Proposal 2', 'Proposal 3'];

var myContract;
web3.eth.defaultAccount = web3.eth.accounts[0];
web3.eth.contract(abi).new(_proposals, { from: web3.eth.accounts[0], gas: 1166180, data: code }, function(err, contract) {
    if (err) {
        console.error(err);
        return;
    } else if (contract.address) {
        myContract = contract;
        console.log('address: ' + myContract.address);
    }
});

var contractName = "Ballot";
Pudding.save(contract_data, './contracts/' + contractName + '.sol.js')
    .then(function() {
        console.log('File ' + './contracts/' + contractName + '.sol.js' + ' was created with the JS contract!');
    });