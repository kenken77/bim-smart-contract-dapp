var BallotContract = require('../contracts/Ballot.sol.js');
var Web3 = require('web3');
var web3 = new Web3();

var provider = new web3.providers.HttpProvider('http://localhost:8545');
web3.setProvider(provider);
BallotContract.setProvider(provider);
BallotContract.defaults({ from: web3.eth.accounts[0] });
var contractAddress = '0x032a84790a5e18be9ac83afabf5cb64a26fcfa64';
var contractInstance = BallotContract.at(contractAddress);

var balanceWei = web3.eth.getBalance(web3.eth.accounts[0]).toNumber();
var balance = web3.fromWei(balanceWei, 'ether');
console.log("Account 0 : " + balance);
var balanceWei = web3.eth.getBalance(web3.eth.accounts[1]).toNumber();
var balance = web3.fromWei(balanceWei, 'ether');
console.log("Account 1 : " + balance);
var balanceWei = web3.eth.getBalance(web3.eth.accounts[2]).toNumber();
var balance = web3.fromWei(balanceWei, 'ether');
console.log("Account 2 : " + balance);

let price = 100 * 1;
console.log(web3.toWei(price, 'ether'));
contractInstance.chairperson().then(function(result) {
    console.log("chairperson -> " + result);
});

contractInstance.getAllProposal().then(function(result) {
    console.log("getAllProposal -> " + result);
});


var account_voter = web3.eth.accounts[0];
var transactionObject = { from: account_voter, value: web3.toWei(15, "ether"), gas: 3000000 };
contractInstance.vote(1, transactionObject);

var account_voter = web3.eth.accounts[1];
var transactionObject = { from: account_voter, value: web3.toWei(15, "ether"), gas: 3000000 };
contractInstance.vote(2, transactionObject);

var voters = contractInstance.voters("0xf4bc05b03da96173683d3e5356be3329ad1c91b2").then(function(result) {
    console.log(" >>> " + result);
});


/*
var voters = contractInstance.voters("0xf4bc05b03da96173683d3e5356be3329ad1c91b2").then(function(result) {
    console.log(" >>> " + result);
});
*/