var Web3 = require('web3');
var fs = require('fs');
var solc = require('solc');

var web3 = new Web3();

var provider = new web3.providers.HttpProvider('http://localhost:8545');
web3.setProvider(provider);
var str2 = web3.fromAscii('Kenneth', 32);
console.log(str2);
web3.eth.accounts.forEach(function(acc) {
    var balanceWei = web3.eth.getBalance(acc).toNumber();
    var balance = web3.fromWei(balanceWei, 'ether');
    console.log(acc + " : " + balance);
})