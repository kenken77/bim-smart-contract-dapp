var GreeterContract = require('../contracts/Greeter.sol.js');
var Web3 = require('web3');
var web3 = new Web3();

var provider = new web3.providers.HttpProvider('http://localhost:8545');
web3.setProvider(provider);
GreeterContract.setProvider(provider);
GreeterContract.defaults({ from: web3.eth.accounts[1] });
var contractAddress = '0x40e0436f5ca65bdbcb764020f8d64f311d3d7445';

test('invoke greeter contract', () => {
    var contractInstance = GreeterContract.at(contractAddress);

    contractInstance.greet.call().then(function(result) {
        console.log(result);
        expect(result).toBe("Hello World!2");
    }).catch(function(err) {
        console.log("Error creating contract!");
        console.log(err.stack);
    });
});