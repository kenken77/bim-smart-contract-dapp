var VotingContract = require('../contracts/Voting.sol.js');
var Web3 = require('web3');
var web3 = new Web3();

var provider = new web3.providers.HttpProvider('http://localhost:8545');
web3.setProvider(provider);
VotingContract.setProvider(provider);
VotingContract.defaults({ from: web3.eth.accounts[1] });
var contractAddress = '0x4e3fb85de129d5f00632aae94e2128a309627fe0';

test('Get votes for Kenneth', () => {
    var contractInstance = VotingContract.at(contractAddress);

    contractInstance.totalVotesFor.call("").then(function(result) {
        console.log(result);
        expect(result).toEqual(1);

    }).catch(function(err) {
        console.log("Error creating contract!");
        console.log(err.stack);
    });
});