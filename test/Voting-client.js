var VotingContract = require('../contracts/Voting.sol.js');
var Web3 = require('web3');
var web3 = new Web3();

var provider = new web3.providers.HttpProvider('http://localhost:8545');
web3.setProvider(provider);
VotingContract.setProvider(provider);
VotingContract.defaults({ from: web3.eth.accounts[0] });
var contractAddress = '0x0af8c0f33478906b09d454cdbb80b35755c1ca9d';
var contractInstance = VotingContract.at(contractAddress);

var balanceWei = web3.eth.getBalance(web3.eth.accounts[0]).toNumber();
var balance = web3.fromWei(balanceWei, 'ether');
console.log("Account 0 : " + balance);
var balanceWei = web3.eth.getBalance(web3.eth.accounts[1]).toNumber();
var balance = web3.fromWei(balanceWei, 'ether');
console.log("Account 1 : " + balance);
var balanceWei = web3.eth.getBalance(web3.eth.accounts[2]).toNumber();
var balance = web3.fromWei(balanceWei, 'ether');
console.log("Account 2 : " + balance);

let price = 100 * 1;
console.log(web3.toWei(price, 'ether'));

contractInstance.getTokenPrice.call().then(function(result) {
    console.log(" getTokenPrice >>>> " + result);
}).catch(function(err) {
    console.log("Error creating contract!" + err);
    console.log(err.stack);
});

contractInstance.getBalanceToken.call().then(function(result) {
    console.log(" getBalanceToken >>>> " + result);
}).catch(function(err) {
    console.log("Error creating contract!" + err);
    console.log(err.stack);
});

var buyObject = {
    value: 25,
    from: web3.eth.accounts[0]
};
contractInstance.buy.call(buyObject).then(function(result) {
    console.log(">>>> " + result);
    web3.eth.getBalance(contractInstance.address, function(error, result) {
        console.log(result);
        console.log(web3.fromWei(result.toString()));
    });
}).catch(function(err) {
    console.log("Error creating contract!" + err);
    console.log(err.stack);
});

contractInstance.buy.call({ value: 5, from: web3.eth.accounts[1] }).then(function(result) {
    console.log(">>>> " + result);
}).catch(function(err) {
    console.log("Error creating contract!" + err);
    console.log(err.stack);
});


contractInstance.buy.call({ value: 25, from: web3.eth.accounts[2] }).then(function(result) {
    console.log(">>>> " + result);
}).catch(function(err) {
    console.log("Error creating contract!" + err);
    console.log(err.stack);
});

//wont work ! buggy !
contractInstance.voteForCandidate('Kenneth', '5', { gas: 140000, from: web3.eth.accounts[0] }).then(function(result) {
    console.log(result);
}).catch(function(err) {
    console.log("Error creating contract!");
    console.log(err.stack);
});

contractInstance.tokensSold.call().then(function(result) {
    console.log("tokensSold >>>>>>" + result);
}).catch(function(err) {
    console.log("Error creating contract!");
    console.log(err.stack);
});

contractInstance.totalVotesFor.call("Kenneth").then(function(result) {
    console.log(result);
}).catch(function(err) {
    console.log("Error creating contract!");
    console.log(err.stack);
});

contractInstance.indexOfCandidate.call("Kenneth").then(function(result) {
    console.log(result);
}).catch(function(err) {
    console.log("Error creating contract!");
    console.log(err.stack);
});

contractInstance.indexOfCandidate.call("Fara").then(function(result) {
    console.log(result);
}).catch(function(err) {
    console.log("Error creating contract!");
    console.log(err.stack);
});

contractInstance.indexOfCandidate.call("Cjin Pheow").then(function(result) {
    console.log(result);
}).catch(function(err) {
    console.log("Error creating contract!");
    console.log(err.stack);
});

contractInstance.indexOfCandidate.call("Bala").then(function(result) {
    console.log(result);
}).catch(function(err) {
    console.log("Error creating contract!");
    console.log(err.stack);
});

contractInstance.indexOfCandidate.call("Bala").then(function(result) {
    console.log(result);
}).catch(function(err) {
    console.log("Error creating contract!");
    console.log(err.stack);
});

contractInstance.tokensSold.call().then(function(result) {
    console.log(result);
}).catch(function(err) {
    console.log("Error creating contract!");
    console.log(err.stack);
});

var candidatesAddress;
contractInstance.allCandidates.call().then(function(result) {
    console.log(result);
    candidatesAddress = result;
}).catch(function(err) {
    console.log("Error creating contract!");
    console.log(err.stack);
});

var kennethAddress;

contractInstance.indexOfCandidate.call("Kenneth").then(function(result) {
    console.log(result.c);
    kennethAddress = candidatesAddress[result.c];
    console.log(kennethAddress);
}).catch(function(err) {
    console.log("Error creating contract!");
    console.log(err.stack);
});

kennethAddress = parseFloat(kennethAddress);
contractInstance.voterDetails.call(kennethAddress).then(function(result) {
    console.log(result);
}).catch(function(err) {
    console.log("Error creating contract!");
    console.log(err.stack);
});