var Web3 = require('web3');
var fs = require('fs');
var solc = require('solc');

var web3 = new Web3();

var provider = new web3.providers.HttpProvider('http://localhost:8545');
web3.setProvider(provider);
var code = fs.readFileSync('contracts/Voting.sol').toString();
//console.log(code);
var compiledCode = solc.compile(code);
//console.log(compiledCode);
var abiDefinition = JSON.parse(compiledCode.contracts[':Voting'].interface);
//console.log(abiDefinition);
var VotingContract = web3.eth.contract(abiDefinition);
//console.log(VotingContract);
var bytecode = compiledCode.contracts[':Voting'].bytecode;
var gasEstimates = compiledCode.contracts[':Voting'].gasEstimates;
console.log(">> " + gasEstimates);
console.log(">> " + bytecode);

VotingContract.new(['Kenneth', 'Fara', 'Bala'], { data: bytecode, from: web3.eth.accounts[0], gas: 4700000 }, function(err, contract) {
    if (err) {
        console.error(err);
        return;
    } else if (contract.address) {
        myContract = contract;
        console.log('address: ' + myContract.address);
        var contractInstance = VotingContract.at(myContract.address);
        console.log(contractInstance.totalVotesFor.call("Kenneth"));
        web3.eth.getGasPrice(function(error, result) {
            var gaslimit = 2000000000000;
            console.log("gaslimit " + gaslimit);
            console.log("gasPrice " + result.toString(10));
            var gasPrice = result;
            console.log(2000000);
            var balance = web3.fromWei(web3.eth.getBalance(web3.eth.accounts[0]));
            console.log("acc 1 balance " + balance);
            var balance2 = web3.fromWei(web3.eth.getBalance(web3.eth.accounts[1]));
            console.log("acc 2 balance2 " + balance2);
            var maxAmount = balance - (gasPrice * gaslimit);
            console.log("maxAmount" + maxAmount);
        });
        console.log(">>> candidates <<< " + contractInstance.totalCandidates.call().toLocaleString());
        contractInstance.donateToCandidate('Kenneth', web3.eth.accounts[2], { gas: 2000000, value: 1000000000000000000, from: web3.eth.accounts[0] });
        console.log("");

        contractInstance.voteForCandidate('Kenneth', { from: web3.eth.accounts[0] });
        var event = contractInstance.DebugVoteForCandidate({ _from: web3.eth.accounts[0] }, { fromBlock: 0, toBlock: 'latest' });

        event.watch(function(error, result) {
            if (!error)
                console.log("wait for a while, check for block Synchronization or block creation");
            console.log(result);
        });
        contractInstance.voteForCandidate('Kenneth', { from: web3.eth.accounts[0] });
        contractInstance.voteForCandidate('Kenneth', { from: web3.eth.accounts[0] });
        contractInstance.voteForCandidate('Fara', { from: web3.eth.accounts[0] });
        contractInstance.voteForCandidate('Bala', { from: web3.eth.accounts[0] });
        console.log("****************************************************************");
        console.log("Kenneth's total votes > " + contractInstance.totalVotesFor.call("Kenneth").toLocaleString());
        console.log("Fara's total votes > " + contractInstance.totalVotesFor.call("Fara").toLocaleString());
        console.log("Bala's total votes > " + contractInstance.totalVotesFor.call("Bala").toLocaleString());
        console.log("****************************************************************");

        web3.eth.accounts.forEach(function(acc) {
            var balanceWei = web3.eth.getBalance(acc).toNumber();
            var balance = web3.fromWei(balanceWei, 'ether');
            console.log(acc + " : " + balance);
        })

        let price = 100 * 1;
        console.log(web3.toWei(price, 'ether'));
    }
});