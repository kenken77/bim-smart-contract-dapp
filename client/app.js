import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import NProgress from 'vue-nprogress'
import { fireInit } from './helpers/firebaseHelpers'
import { sync } from 'vuex-router-sync'
import App from './App.vue'
// import VueResource from 'vue-resource'
// import VueRouter from 'vue-router'

import VueFire from 'vuefire'
// import firebase from 'firebase'
import router from './router'
import store from './store'
import * as filters from './filters'
import { TOGGLE_SIDEBAR } from 'vuex-store/mutation-types'

Vue.router = router
Vue.use(VueAxios, axios)
Vue.use(NProgress)
// Vue.use(VueRouter)
Vue.use(VueFire)
// Vue.use(VueResource)

fireInit(fireStatus)
var authenticated = false
console.log(authenticated)
    // Enable devtools
Vue.config.devtools = true

sync(store, router)
// Vue.http.options.root = 'http://localhost:3002'
// Vue.http.headers.common['Access-Control-Allow-Origin'] = '*'
// Vue.http.headers.common['Access-Control-Request-Method'] = '*'

const nprogress = new NProgress({ parent: '.nprogress-container' })

const { state } = store

router.beforeEach((route, redirect, next) => {
  if (state.app.device.isMobile && state.app.sidebar.opened) {
    store.commit(TOGGLE_SIDEBAR, false)
  }
  next()
})

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

const app = new Vue({
  router,
  store,
  nprogress,
  ...App
})

function fireStatus (loggedIn, user) {
  if (loggedIn) {
    authenticated = true
  } else {
    authenticated = false
  }
}

export { app, router, store, authenticated }
