var web3extended = require('web3_ipc')
var options = {
  host: 'http://localhost:8545',
  ipc: false,
  personal: true,
  admin: true,
  debug: false
}
export default web3extended.create(options)
