import * as types from '../../mutation-types'
import lazyLoading from './lazyLoading'
import menuData from './menu.json'
// show: meta.label -> name
// name: component name
// meta.label: display label

const state = {
  items: getMenuItems()
}

const mutations = {
  [types.EXPAND_MENU] (state, menuItem) {
    if (menuItem.index > -1) {
      if (state.items[menuItem.index] && state.items[menuItem.index].meta) {
        state.items[menuItem.index].meta.expanded = menuItem.expanded
      }
    } else if (menuItem.item && 'expanded' in menuItem.item.meta) {
      menuItem.item.meta.expanded = menuItem.expanded
    }
  }
}

function getMenuItems () {
  var items = menuData
  items.forEach(function (item) {
    item.component = lazyLoading(item.componentName, true)
  })
  return items
}

export default {
  state,
  mutations
}
