import firebase from 'firebase'

export function fireInit (func) {
  var config = {
    apiKey: 'AIzaSyDNCDhENbH0UpCNwq-4886L6jiUtGLo4Vk',
    authDomain: 'construction-smartcontract.firebaseapp.com',
    databaseURL: 'https://construction-smartcontract.firebaseio.com/',
    storageBucket: 'construction-smartcontract.appspot.com'
  }
  firebase.initializeApp(config)

  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      func(true, user)
    } else {
      func(false)
    }
  }, function (error) {
    console.log(error)
  })
}
