# Blockchain ins JS
```
npm install
HTTP_PORT=3001 P2P_PORT=6001 nodemon blockchain.js
HTTP_PORT=3002 P2P_PORT=6002 PEERS=ws://localhost:6001 nodemon blockchain.js
curl -H "Content-type:application/json" --data '{"data" : "Some data to the first block"}' http://localhost:3001/mineBlock

curl http://localhost:3001/blocks
curl -H "Content-type:application/json" --data '{"data" : "Some data to the first block"}' http://localhost:3001/mineBlock

curl -H "Content-type:application/json" --data '{"peer" : "ws://localhost:6001"}' http://localhost:3001/addPeer

curl http://localhost:3001/peers
```
# bim-smart-contract-dapp

> A Vue.js project that covers bim and smart contract + web3.js

> Webpack and http server

#Commit message naming convention

> (deployment)
> (frontend)
> (backend)
> (database)
> (e2e)
> (utest)

#Smart Contract IDE (REMIX)
> http://browser-solidity.stackup.sg:8080/

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

## Smart contract EC2 Ubuntu setup
```
# install nodeJS
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs

# install build essential tools and python
sudo apt-get install build-essential python

# make directory, instakk rpc, web3 and run the ethereum test rpc
mkdir bim-smart-contract
cd bim-smart-contract/
npm install ethereumjs-testrpc web3
node_modules/.bin/testrpc -l 2000000000000 -g 1
# diffrent console
npm install solc --save 

sudo apt-get install software-properties-common
sudo add-apt-repository -y ppa:ethereum/ethereum
sudo apt-get update

# install solc
sudo add-apt-repository ppa:ethereum/ethereum
sudo apt-get update
sudo apt-get install solc
Address: {e87dee872783c322eb7551f188907ffea5e8a90d}

```

### Windows Setup

> Install Visual Studio Community Edition. If you choose a custom installation, the bare minimum items that need to be checked are all pertaining to Visual C++ (Current version is VS 2017)

> Install the Windows SDK for Windows (If you are in Windows 10 install SDK for Windows 10)

> Install Python 2.7 if not already installed, and make sure to add its install location to your PATH.

> Install OpenSSL from here. Make sure to choose the right package for your architecture, and only install the full package (not the light version). You must install OpenSSL at its recommended location — do not change the install path.

> Download and install node v8.1.2. Version v6.11.0 is not recommended with VS2017

> Execute the command npm install ethereumjs-testrpc web3

> Download the solc https://github.com/ethereum/solidity/releases

> https://github.com/fivedogit/solidity-baby-steps

### Start the test ethereum rpc
```
node_modules\.bin\testrpc -l 2000000000000 -g 1
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
