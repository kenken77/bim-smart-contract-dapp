var express = require("express");
var bodyParser = require('body-parser');
var WebSocket = require("ws");
var Web3 = require('web3');
var fs = require('fs');
var solc = require('solc');
var cors = require('cors');

var http_port = process.env.HTTP_PORT || 3002;
var web3 = new Web3();

var provider = new web3.providers.HttpProvider('http://localhost:8545');
web3.setProvider(provider);
var code = fs.readFileSync('contracts/Voting.sol').toString();
console.log(code);
var compiledCode = solc.compile(code);
console.log(compiledCode);
var abiDefinition = JSON.parse(compiledCode.contracts[':Voting'].interface);
console.log(abiDefinition);
var VotingContract = web3.eth.contract(abiDefinition);
console.log(VotingContract);
var bytecode = compiledCode.contracts[':Voting'].bytecode;
var gasEstimates = compiledCode.contracts[':Voting'].gasEstimates;
console.log(">> " + gasEstimates);
console.log(">> " + bytecode);
var votingContractAddress;

var initHttpServer = () => {
    var app = express();
    app.use(bodyParser.json());

    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    app.post('/create-contract', cors(), (req, res) => {
        console.log("CREATE !" + req);
        console.log(JSON.stringify(req.body));
        var candidates = JSON.parse(JSON.stringify(req.body));
        var candidate_list = []
        candidates.forEach(function(item) {
            console.log(item.candidateInfo.candidateName);
            candidate_list.push(item.candidateInfo.candidateName);
        })
        console.log(candidate_list);
        VotingContract.new([candidate_list[0], candidate_list[1], candidate_list[2], candidate_list[3]], { data: bytecode, from: web3.eth.accounts[0], gas: 4700000 }, function(err, contract) {
            if (err) {
                console.error(err);
                return;
            } else if (contract.address) {
                myContract = contract;
                votingContractAddress = myContract.address;
                console.log('address: ' + myContract.address);
                res.json({ contractAddr: myContract.address });
            }
        });
    });

    app.post('/vote', cors(), (req, res) => {
        console.log("Voting !!!!!!");
        console.log(req.body);
        console.log(JSON.stringify(req.body));
        var candidate = JSON.parse(JSON.stringify(req.body));
        var contractInstance = VotingContract.at(votingContractAddress);
        contractInstance.voteForCandidate(candidate.candidateName, { from: web3.eth.accounts[0] });
        res.json(candidate);
    });

    app.post('/donate', cors(), (req, res) => {
        console.log("Donate !!!!!!");
        console.log(req.body);
        console.log(JSON.stringify(req.body));

        var contractInstance = VotingContract.at(votingContractAddress);
        var candidate = JSON.parse(JSON.stringify(req.body));
        var toAccount;
        web3.eth.accounts.forEach((item, index) => {
            if (item === candidate.candidateAccount) {
                console.log("to account match !" + item);
                toAccount = index;
            }
        })
        console.log("candidate.candidateFund = " + candidate.candidateFund);
        var fundTobeXfer = parseInt(candidate.candidateFund) * 1000000000000000000;
        console.log("fundTobeXfer = " + fundTobeXfer);
        console.log("candidate.candidateName = " + candidate.candidateName);
        console.log("toAccount = " + toAccount);
        var candidateName = new String(candidate.candidateName);
        console.log(web3.eth.accounts[1])
        console.log(web3.eth.accounts[parseInt(toAccount)])
        console.log(candidateName.toString())
        var x = web3.fromAscii(candidateName.toString());
        contractInstance.donateToCandidate(x, web3.eth.accounts[parseInt(toAccount)], {
            gas: 2000000,
            value: 1000000000000000000,
            from: web3.eth.accounts[0]
        });
        res.json(candidate);
    });

    app.get('/total-votes', cors(), (req, res) => {
        console.log("Total Votes !!!!!!");
        console.log(req.body);
        console.log(JSON.stringify(req.body));
        var candidate = JSON.parse(JSON.stringify(req.body));
        var candidateTotalVotes = contractInstance.totalVotesFor.call(candidate.candidateName).toLocaleString();
        console.log(candidateTotalVotes);
        res.json(candidateTotalVotes);
    });

    app.get('/accounts', cors(), (req, res) => {
        var ethAccounts = web3.eth.accounts;
        console.log(ethAccounts);
        res.json(ethAccounts);
    });

    app.listen(http_port, () => console.log('Listening http on port: ' + http_port));
};

initHttpServer();