'use strict';
let fs = require("fs");
let Web3 = require('web3');
var Pudding = require('ether-pudding');
var solc = require('solc');

var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
//console.log(web3.isConnected());

let source = fs.readFileSync("contracts/voting.json");
console.log(">>>" + source);

let contracts = JSON.parse(source)["contracts"];
console.log(contracts);
let abi = JSON.parse(contracts['Voting.sol:Voting'].abi);

// Smart contract EVM bytecode as hex
let code = '0x' + contracts['Voting.sol:Voting'].bin;

var contract_data = {
    abi: abi,
    binary: code
};

console.log(web3.eth);
var _candidates = ['Fara', 'Kenneth', 'CjinPheow'];
//var pricePerToken = web3.toWei('0.1', 'ether');
var myContract;

web3.eth.contract(abi).new(_candidates, { from: web3.eth.accounts[0], gas: 4700000, data: code }, function(err, contract) {
    if (err) {
        console.error(err);
        return;
    } else if (contract.address) {
        myContract = contract;
        console.log('address: ' + myContract.address);
    }
});

var contractName = "Voting";
Pudding.save(contract_data, './contracts/' + contractName + '.sol.js')
    .then(function() {
        console.log('File ' + './contracts/' + contractName + '.sol.js' + ' was created with the JS contract!');
    });