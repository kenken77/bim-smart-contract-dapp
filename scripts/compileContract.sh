#!/bin/bash
SOL_FILE=$1
SOL_JSON=$2
solc $SOL_FILE --combined-json abi,asm,ast,bin,bin-runtime,clone-bin,devdoc,interface,opcodes,srcmap,srcmap-runtime,userdoc > $SOL_JSON
