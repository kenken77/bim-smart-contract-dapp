pragma solidity ^0.4.11; //We have to specify what version of the compiler this code will use

contract Voting {

  mapping (bytes32 => uint8) public votesReceived;
  mapping (bytes32 => address) public donationReceived;
  mapping (bytes32 => address) public fromWhichDonor;
  address public donor;

  bytes32[] public candidateList;
  event DebugCandidates(bytes32 x, bytes32 y, bytes32 z);
  event DebugValidCandidate(bool x);
  event DebugVoteForCandidate(uint8 x);
  bool isStartedVotingOver = false;

  // Initialize all the contestants
  function Voting(bytes32[] candidateNames) {
    candidateList = candidateNames;
    DebugCandidates(candidateList[0], candidateList[1], candidateList[2]);
  }

  function totalVotesFor(bytes32 candidate) returns (uint8) {
    if (validCandidate(candidate) == false) throw;
    return votesReceived[candidate];
  }

  function totalCandidates() returns (uint) {
    return candidateList.length;
  }

  function donateToCandidate(bytes32 candidate, address account) payable{
    if (validCandidate(candidate) == false) throw;
    donationReceived[candidate] = account;
    donor = msg.sender;
    fromWhichDonor[candidate]= donor;
    account.transfer(msg.value);
  }

  function startVoting(bool votingStarted) {
      isStartedVotingOver = votingStarted;
  }

  function isVotingStarted() returns (bool) {
      return isStartedVotingOver;
  }

  function voteForCandidate(bytes32 candidate) {
    DebugCandidates(candidate, candidate, candidate);
    if (validCandidate(candidate) == false) throw;
    votesReceived[candidate] += 1;
    DebugVoteForCandidate(votesReceived[candidate]);
  }

  function validCandidate(bytes32 candidate) returns (bool) {
    for(uint i = 0; i < candidateList.length; i++) {
      if (candidateList[i] == candidate) {
        DebugValidCandidate(true);
        return true;
      }
    }
    return false;
  }
}